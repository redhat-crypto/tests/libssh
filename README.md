libssh
======

This is a legacy repo for some libssh tests. The tests were migrated to the
original repo and they will be maintained there.

For more information or for concerns please contact: szidek@redhat.com or gpantela@redhat.com